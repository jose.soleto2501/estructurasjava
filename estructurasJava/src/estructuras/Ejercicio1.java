package estructuras;

import java.util.HashSet;

public class Ejercicio1 {

    public static void main(String[] args) {

        HashSet<Cliente> cuentaClientes = new HashSet<Cliente>();

        Cliente cliente1 = new Cliente("Jose Soleto", "12345678A", 0.15);
        Cliente cliente2 = new Cliente("Fran Martínez", "12345678B", 3.25);
        Cliente cliente3 = new Cliente("Sergi Almería", "12345678C",3204 );
        Cliente cliente4 = new Cliente("Immanuel Dominic", "12345678D", 6.5);
        Cliente cliente5 = new Cliente("Oleg García", "12345678E", 7000);

        cuentaClientes.add(cliente1);
        cuentaClientes.add(cliente2);
        cuentaClientes.add(cliente3);
        cuentaClientes.add(cliente4);
        cuentaClientes.add(cliente5);

        for (Cliente c : cuentaClientes){
            System.out.println(c);
        }

    }

}