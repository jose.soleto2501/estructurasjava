package estructuras;


import java.util.Objects;

public class Cliente {
    //ATRIBUTOS DE LA CLASE CLIENTE
    private String nombre;
    private String DNI;
    private Double saldo;


    //CONSTRUCTOR DE LA CLASE CLIENTE
    public Cliente(String nombre, String DNI, double dinero) {
        setNombre(nombre);
        setDNI(DNI);
        setSaldo(dinero);
    }

    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    public String getDNI() {
        return DNI;
    }


    public void setDNI(String dNI) {
        DNI = dNI;
    }
    //METODO PARA COMPROBAR SI SE REPTIE DNI
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(DNI, cliente.DNI);
    }

    @Override
    public int hashCode() {
        return Objects.hash(DNI);
    }

    public Double getSaldo() {
        return saldo;
    }


    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
    //METODO TO STRING
    @Override
    public String toString() {

        return "NOMBRE:	" + getNombre() + " DNI: " + getDNI() + "  SALDO: " + getSaldo() + " €";

    }



}
